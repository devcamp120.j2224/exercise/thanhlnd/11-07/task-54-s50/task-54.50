package com.example.demo;

import java.util.ArrayList;

public class Country {
    String countryCode ;
    String countryName ;
    ArrayList <Region> regions ;

    // constructor
    // thêm 2 tham số cũng dc , vì xuống dưới toString ta sẽ set Override nó để đè lên sau 
    public Country(String countryCode , String countryName){
        this.countryCode = countryCode ;
        this.countryName = countryName ;
    }

    public String getCountryCode(){
        return countryCode;
    }
    public void setCountryCode (String countryCode){
        this.countryCode = countryCode ;
    }

    public String getCountryName(){
        return countryName;
    }
    public void setCountryName (String countryName){
        this.countryName = countryName ;
    }

    public ArrayList <Region> getRegions(){
        return regions;
    }
    public void setRegion (ArrayList <Region> regions){
        this.regions = regions ;
    }

    @Override
    public String toString(){
        return "Country [ countryCode : " + this.countryCode + " , countryName : " + this.countryName + " , regions : " + this.regions + "]";
    }
}
